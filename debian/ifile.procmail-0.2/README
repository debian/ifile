ifile.procmail
==============

This package contains shell scripts that allows ifile to be used in standard
unix e-mail environment - i.e. with procmail and standard unix mailboxes
(mbox).

Q: What is ifile? How does it work?
A: Visit http://www.nongnu.org/ifile/ and read it.

Shortly: You can use it for e-mail filtering (automatic sorting emails
to right folders). I use it for SPAM filtering.
It counts word statistics in your e-mails in different folders and uses
it for sorting new emails into them.


Requires:
---------
bash, procmail, formail, mktemp.
And of course ifile itself - this version works with ifile 1.0.3 (older has
different command line switches).


Installation:
-------------
Get ifile from http://www.ai.mit.edu/%7Ejrennie/ifile/ and install it.

Copy all executable scripts in bin/ subfolder into some directory where
could be used system wide.

Example:
# cp bin/* /usr/local/bin/


Setup:
------
Log in as a user which receives the e-mails.

Run ifile.reset to reset the configuration (~/.idata file).

Say you have your mbox folder in directory ~/mail/. Say you wan't to
just recognize "good" messages and "spam" messages. So you've got some
folders with "good" emails and one folder with "spam" messages. So you
need to learn ifile how to recognize between them - simply tell it where
"good" emails are and where "spam" emails are:
$ ifile.learn.mailbox good /var/spool/mail/your_mbox ~/mail/mbox
$ ifile.learn.mailbox good ~/mail/alerts ~/mail/bugtraq ~/mail/love
$ ifile.learn.mailbox spam ~/mail/spam

More messages (larger mboxes) you learn - more accurate filtering you
get. You can test it with:
$ ifile.test.mailbox good ~/mail/mbox
$ ifile.test.mailbox spam ~/mail/spam

If you're not satisfied with results - learn more/less/different
mailboxes or simply don't use this system. Sometimes it helps to not
learn some specific mailboxes or to learn more mailboxes... It just
depends on sort of emails you get.
If you're satisfied with the results you can setup mail delivery to use
it.

We need your local mail server (delivery) to be proceeded through
procmail. This is default on most Linux systems but it couldn't be on
yours. Consult your administrator, or check it by yourself. 
(If you're sure it is not proceeded through procmail you can use
"|/path/to/procmail" in ~/.forward or "|preline /path/to/procmail" in
~/.qmail file)

So you want to configure procmail to use ifile. Edit the file
~/.procmailrc to look something like sample.procmailrc file. For further
information see man procmail, man procmailrc, man procmailex. Be
carefull, having bad ~/.procmailrc file can make your e-mails lost!

Now you're done - e-mails should be sorted with the help of ifile.

Usage:
------
Sometimes you want to tell ifile that it made a "mistake" and sorted
e-mail into bad folder. You can do it by hand and filter the message
through ifile.relearn.message script. Say you received spam message but
ifile didn't recognize it and sorted it as a good message. So you can do
$ ifile.relearn.message good spam <relevant_message_file

I use Mutt as a mail client and this is very simplified with those
macros in .muttrc:

folder-hook . 'macro pager <f8> "|ifile.relearn.message good spam\ns=spam\n\n\t" "Relearn a spam and store it to spam folder" '
folder-hook spam 'macro pager <f8> "|ifile.relearn.message spam good\ns=mbox\n\n\t" "Relearn good message and store it to mbox folder" '

So when I hit F8 ifile relearns it and move it into the right folder.


License:
--------
Public domain.



Enjoy!

Martin Ma�ok <martin.macok@underground.cz>
http://Xtrmntr.org/ORBman/
