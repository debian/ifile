/* header file for self-resizing hash table */
/* written by Jason Rennie <jr6b+@andrew.cmu.edu> */

#define CROWDED_PCT 0.50

typedef struct _hash_elem {
  void * index;
  void * entry;
} hash_elem;

typedef struct _htable {
  int num_slots;                 /* size of DATA array */
  int num_entries;               /* # of slots in DATA array being used */
  hash_elem * data;              /* array of HASH_ELEMs */
  /* S is pointer to index string.  SIZE is size of htable */
  unsigned long (*hash) (const void * s, long int size);
} htable;

void htable_init(htable * hash_table, long int num_slots,
		 unsigned long (*hash_fun) (const void *, long int));
void htable_free(htable * hash_table, 
	    void (*free_index)(void *), void (*free_entry)(void *));
void htable_put(htable * hash_table, void * index, void * entry);
void * htable_lookup(htable * hash_table, void * index);
hash_elem * htable_init_traversal(htable * hash_table);
hash_elem * htable_next_traversal(htable * hash_table, hash_elem * elem);
void htable_resize(htable * hash_table, long int num_slots);


void htable_free_guts(htable * hash_table, 
		      void (*free_index)(void *), void (*free_entry)(void *));
