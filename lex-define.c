/* this file defines the default lexers and initializes them */

/* ifile - intelligent mail filter for EXMH/MH
   ifile is Copyright (C) 1997  Jason Rennie <jrennie@ai.mit.edu>
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program (see file 'COPYING'); if not, write to the Free
   Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA  02111-1307, USA.
   */

#include <ifile.h>

extern arguments args;

static ifile_lexer_simple _ifile_default_lexer_simple;
static ifile_lexer_email _ifile_default_email_lexer;

ifile_lexer_simple *ifile_default_lexer_simple;
ifile_lexer_email *ifile_default_email_lexer;

char *ifile_email_headers_to_keep[4] = { "subject", "from", "to", NULL };

/* The default lexer used by all library functions. */
/* NOTE: Be sure to set this to a value, otherwise some linkers (like
   SunOS's) will not actually include this .o file in the executable,
   and then _ifile_default_lexer() will not get called, and then the
   lexer's will not get initialized properly.  Ug. */
ifile_lexer *ifile_default_lexer = (void*)-1;

/* adapted from libbow by Jason Rennie <jrennie@ai.mit.edu> */
void 
ifile_default_lexer_init ()
{
  if (args.lexer == WHITE_LEXER)
    {
      _ifile_default_lexer_simple = *ifile_white_lexer;
      ifile_verbosify(ifile_debug, "Chose white lexer.\n");
    }
  else if (args.lexer == ALPHA_ONLY_LEXER)
    {
      _ifile_default_lexer_simple = *ifile_alpha_only_lexer;
      ifile_verbosify(ifile_debug, "Chose alpha only lexer.\n");
    }
  else /* args.lexer == ALPHA_LEXER */
    {
      _ifile_default_lexer_simple = *ifile_alpha_lexer;
      ifile_verbosify(ifile_debug, "Chose alpha lexer.\n");
    }
  
  ifile_default_lexer_simple = &_ifile_default_lexer_simple;

  /* these need to come AFTER the default lexer is set */
  if (args.stemming)
    ifile_default_lexer_simple->stem_func = ifile_stem_porter;
  else
    ifile_default_lexer_simple->stem_func = NULL;

  if (args.stoplist)
    ifile_default_lexer_simple->stoplist_func = ifile_stoplist_present;
  else
    ifile_default_lexer_simple->stoplist_func = NULL;

  _ifile_default_email_lexer = *ifile_email_lexer;
  ifile_default_email_lexer = &_ifile_default_email_lexer;

  ifile_default_email_lexer->indirect_lexer.underlying_lexer =
    (ifile_lexer *) ifile_default_lexer_simple;

  ifile_default_lexer = (ifile_lexer *) ifile_default_email_lexer;

  if (args.skip_header)
    ifile_default_email_lexer->headers_to_keep = ifile_email_headers_to_keep;
  else
    /* keep all headers */
    ifile_default_email_lexer->headers_to_keep = (char **) -1;
}
