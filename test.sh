#!/bin/sh

./configure --prefix=/tmp &> /dev/null
make &> /dev/null
make install &> /dev/null
if [ -f /tmp/man/man1/ifile.1 ]; then echo ok 1; else echo not ok 1; fi

./configure --prefix=/tmp --exec-prefix=/tmp/linux &> /dev/null
make &> /dev/null
make install &> /dev/null
if [ -f /tmp/linux/bin/ifile ]; then echo ok 2; else echo not ok 2; fi

./configure --bindir=/tmp/foobin --mandir=/tmp/fooman &> /dev/null
make &> /dev/null
make install &> /dev/null
if [ -f /tmp/foobin/ifile ]; then echo ok 3; else echo not ok 3; fi
if [ -f /tmp/foobin/ifilter.mh ]; then echo ok 4; else echo not ok 4; fi
if [ -f /tmp/foobin/irefile.mh ]; then echo ok 5; else echo not ok 5; fi
if [ -f /tmp/foobin/knowledge_base.mh ]; then echo ok 6; else echo not ok 6; fi
if [ -f /tmp/foobin/news2mail ]; then echo ok 7; else echo not ok 7; fi
if [ -f /tmp/fooman/man1/ifile.1 ]; then echo ok 8; else echo not ok 8; fi
