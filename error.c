/* This file is error.c - library for message and error reporting */

/* ifile - intelligent mail filter for EXMH/MH
   Copyright (C) 1997  Jason Rennie <jrennie@ai.mit.edu>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
   */

#include <stdarg.h>     /* va_start(), va_end(), va_list */
#include <stdio.h>      /* fopen(), fclose() */
#include <stdlib.h>
#include <string.h>
#include <errno.h>      /* required by SunOS 5.5.1/gcc-2.7.2 for errno var */
#include <time.h>       /* for srand(time(NULL)) call */
#include <sys/stat.h>   /* for chmod() call */

#include <ifile.h>

extern arguments args;

FILE * INFO;

/* Returns a newly allocated character array which contains the
   portion of FULL_PATH postceeding the last '/' in FULL_PATH,
   or the entire string if FULL_PATH contains no '/' characters */
/* written by Jason Rennie <jrennie@ai.mit.edu> for ifile */
char *
ifile_strip_path (char * full_path)
{
  char buf[MAX_STR_LEN];
  int buf_index = 0;       /* index of last character in BUF */
  int full_path_index = 0; /* index of current character in FULL_PATH */
  char * executable_name;  /* pointer to string to be returned */

  while (full_path[full_path_index])
    {
      buf[buf_index++] = full_path[full_path_index++];
      if (full_path[full_path_index] == '/')
	{
	  buf_index = 0;
	  full_path_index++;
	}
    }
  buf[buf_index] = '\0';
  executable_name = malloc(strlen(buf)+1);
  strcpy(executable_name, buf);

  return executable_name;
}

/* Opens info log file to report information/errors (if requested) */
/* Written by Jason Rennie <jrennie@ai.mit.edu> for ifile */
FILE *
ifile_open_log (int argc, char ** argv)
{
  char *info_file;

  /* if no temp file is to be created, immediately return */
  if (!args.tmp_file) return NULL;

  srand(time(NULL));

  /* Create string with full path for ~/.ifile_log */
  info_file = ifile_sprintf("%s/.ifile.log", getenv("HOME"));

  if ((INFO = fopen(info_file, "w")))
    {
      chmod(info_file, 0600);
      ifile_verbosify(ifile_progress, "%s called\n", IFILE_VERSION);
      
      ifile_verbosify(ifile_verbose, "Successfully opened log file: %s\n",
		      info_file);
      return INFO;
    }
  else
    {
      ifile_verbosify(ifile_quiet, "Not able to open log file: %s  Error: %d\n", info_file, errno);
      return NULL;
    }
}


/* Closes info log file if necessary */
/* Written by Jason Rennie <jrennie@ai.mit.edu> for ifile */
void
ifile_close_log ()
{
  if (!args.tmp_file) return;

  fclose(INFO);
  ifile_verbosify(ifile_verbose, "Closed log file.\n");
}


/* Print the printf-style FORMAT string and arguments on STDERR, only if
   ARGS->VERBOSITY is equal or greater than the argument VERBOSITY_LEVEL. */
/* Written by Jason Rennie <jrennie@ai.mit.edu> for ifile
   adapted from bow_verbosify() - written by Andrew Kachites McCallum */
int
ifile_verbosify (int verbosity_level, const char *format, ...)
{
  int ret = 0;
  va_list ap;

  /* The following few lines makes it possible to see the priority
     levels of the various lines of output */

  if (verbosity_level <= args.verbosity)
    {
      va_start (ap, format);
      ret = vfprintf (stderr, format, ap);
      va_end (ap);
    }
  fflush (stderr);

  if ((verbosity_level <= args.verbosity) ||
      (verbosity_level <= ifile_progress))
    {
      va_start (ap, format);
      if (INFO != NULL) vfprintf (INFO, format, ap);
      va_end (ap);
    }
  if (INFO != NULL) fflush (INFO);

  return ret;
}


/* Reports an error in printf style format and halts the program */
/* strerror() is currently not used to report errors because Sun machines
   will not compile the function for some reason */
/* Written by Jason Rennie <jrennie@ai.mit.edu> for ifile
   adapted from bow_error() - written by Andrew Kachites McCallum */
void
ifile_error (const char *format, ...)
{
  va_list ap;

  va_start (ap, format);
  if (INFO) vfprintf (INFO, format, ap);
  va_end (ap);
  /*  fprintf("Error: %s\n", strerror(errno)); */

  va_start (ap, format);
  vfprintf (stderr, format, ap);
  va_end (ap);
  /*  fprintf(stderr, "Error: %s\n\n", strerror(errno)); */

  ifile_close_log(INFO);

  exit (-1);
}
